def revers(text):
    ...


if __name__ == "__main__":
    cases = (
        ("", ""),
        ("abcd", "dcba"),
        ("abcd efgh", "dcba hgfe"),
        ("ab3cdef", "fe3dcba"),
    )
    for arg, res in cases:
        assert revers(arg) == res